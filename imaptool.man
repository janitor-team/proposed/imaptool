.TH IMAPTOOL 1
.\" NAME should be all caps, SECTION should be 1-8, maybe w/ subsection
.\" other parms are allowed: see man(7), man(1)
.SH NAME
imaptool \- tool for creating client-side image maps
.SH SYNOPSIS
.B imaptool
.I "<.gif|.jpeg file>"
.br
.SH "DESCRIPTION"
This manual page documents briefly the
.BR imaptool
command.
This manual page was written for the Debian GNU/Linux distribution
because the original program does not have a manual page.
.PP
.B imaptool
is a tool that helps in the creation of client-side image maps.
.PP
For more information on client-side image maps refer see -
http://home.netscape.com/assist/net_sites/html_extensions_3.html
.PP
.B imaptool
is pretty easy to use.
.PP
Invoke
.B imaptool
on a .gif or .jpeg image.  Click on 'Shape' and choose
a shape (Available shapes - Rectangle, Circle and Polygon).  Click on the
image and drag the mouse to enclose the required area.  The HTML tag for
this client-side image map area is now in your X buffer.
.PP
Once you have enclosed the required area, change to your favorite editor
(running in another window) and click Button 2 to paste the client-side
image map tag.
.PP

.SH AUTHOR
This manual page was written by Sudhakar Chandrasekharan <thaths@netscape.com>,
for the Debian GNU/Linux system (but may be used by others).

