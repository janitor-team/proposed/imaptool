#Do you want to use jpeg-library???
JPEG_INCLUDE=-I/usr/include/
JPEG_CHOICE=-DUSE_JPEG_LIB $(JPEG_INCLUDE)
#JPEG_CHOICE=
LIBRARIES=-lXt -lXaw -lXmu -lXext -lX11 -ljpeg -lm
#LIBRARIES=-lXt -lXaw -lXmu -lXext -lX11 -lm

CC=gcc -I/usr/X11R6/include/
CFLAGS=-g -Wall

LIBDIR=-L/usr/lib -L/usr/X11R6/lib
OBJECTS=imaptool.o file2pixmap.o dgif_lib.o gif_err.o gifalloc.o
imaptool:	$(OBJECTS)
		$(CC) -g -o imaptool $(LDFLAGS) $(LIBDIR) $(OBJECTS) $(LIBRARIES)
imaptool.o:	imaptool.c icon iconmask
		$(CC) $(CPPFLAGS) $(CFLAGS) -c imaptool.c
file2pixmap.o:	file2pixmap.c
		$(CC) $(CPPFLAGS) $(CFLAGS) -c file2pixmap.c $(JPEG_CHOICE)
dgif_lib.o:	dgif_lib.c
		$(CC) $(CPPFLAGS) $(CFLAGS) -c dgif_lib.c
gif_err.o:	gif_err.c
		$(CC) $(CPPFLAGS) $(CFLAGS) -c gif_err.c
gifalloc.o:	gifalloc.c
		$(CC) $(CPPFLAGS) $(CFLAGS) -c gifalloc.c

clean:
	rm -f $(OBJECTS) imaptool
